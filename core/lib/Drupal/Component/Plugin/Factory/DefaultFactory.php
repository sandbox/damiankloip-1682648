<?php
/**
 * @file
 * Definition of Drupal\Component\Plugin\Factory\DefaultFactory.
 */

namespace Drupal\Component\Plugin\Factory;

use Drupal\Component\Plugin\Discovery\DiscoveryInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Derivative\DerivativeInterface;

/**
 * Default plugin factory.
 *
 * Instantiates plugin instances by passing the full configuration array as a
 * single constructor argument. Plugin types wanting to support plugin classes
 * with more flexible constructor signatures can do so by using an alternate
 * factory such as Drupal\Component\Plugin\Factory\ReflectionFactory.
 */
class DefaultFactory implements FactoryInterface {

  /**
   * @todo
   *
   * @var Drupal\Component\Plugin\Discovery\DiscoveryInterface
   */
  protected $discovery;

  /**
   * @todo
   *
   * @var string
   */
  protected $classKey;

  /**
   * Constructs a Drupal\Component\Plugin\Factory\DefaultFactory object.
   */
  public function __construct(DiscoveryInterface $discovery, $class_key = 'class') {
    $this->discovery = $discovery;
    $this->classKey = $class_key;
  }

  /**
   * Implements Drupal\Component\Plugin\Factory\FactoryInterface::createInstance().
   */
  public function createInstance($plugin_id, array $configuration) {
    $plugin_class = $this->getPluginClass($plugin_id);
    return new $plugin_class($configuration, $plugin_id, $this->discovery);
  }

  /**
   * Finds the class relevant for a given plugin.
   *
   *  @param array $plugin_id
   *    The id of a plugin.
   *
   *  @return string
   *    The appropriate class name.
   */
  protected function getPluginClass($plugin_id) {
    if (empty($this->classKey)) {
      throw new PluginException('The plugin manager did not specify a valid key for determining the plugin instance class.');
    }

    $plugin_definition = $this->discovery->getDefinition($plugin_id);
    if (empty($plugin_definition[$this->classKey])) {
      throw new PluginException('The plugin did not specify an instance class.');
    }

    $class = $plugin_definition[$this->classKey];

    if (!class_exists($class)) {
      throw new PluginException(sprintf('Plugin instance class "%s" does not exist.', $class));
    }

    return $class;
  }
}
