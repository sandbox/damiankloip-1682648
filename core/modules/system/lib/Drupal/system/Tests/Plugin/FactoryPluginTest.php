<?php

/**
 * @file
 * Definition of Drupal\system\Tests\Plugin\PluginTest
 */

namespace Drupal\system\Tests\Plugin;

use Drupal\plugin_test\Plugin\TestPluginManager;
use Drupal\plugin_test\Plugin\MockBlockManager;
use Drupal\Component\Plugin\Exception\ExceptionInterface;
use Exception;

/**
 * Tests the plugin system.
 */
class FactoryPluginTest extends PluginTestBase {
  protected $testPluginManager;
  protected $mockBlockManager;

  public static function getInfo() {
    return array(
      'name' => 'Factory Tests',
      'description' => 'Test the base plugin factories.',
      'group' => 'Plugin API',
    );
  }

  public function setUp() {
    parent::setUp();

    // Real modules implementing plugin types may expose a module-specific API
    // for retrieving each type's plugin manager, or make them available in
    // Drupal's dependency injection container, but for unit testing, we get
    // the manager directly.

    // A very basic mock plugin manager.
    $this->testPluginManager = new TestPluginManager();

    // A slightly more advanced plugin manager mocking a block system with
    // derivatives.
    $this->mockBlockManager = new MockBlockManager();
  }

  /**
   * Test that DefaultFactory can create a plugin instance.
   */
  function testDefaultFactory() {
    // Ensure a non-derivative plugin can be instantiated.
    $plugin = $this->testPluginManager->createInstance('user_login', array('title' => 'Please enter your login name and password'));
    $this->assertIdentical(get_class($plugin), 'Drupal\plugin_test\Plugin\plugin_test\mock_block\MockUserLoginBlock', 'Correct plugin class instantiated with default factory.');
    $this->assertIdentical($plugin->getTitle(), 'Please enter your login name and password', 'Plugin instance correctly configured.');

    // Ensure that attempting to instantiate non-existing plugins throws a
    // PluginException.
    try {
      $this->testPluginManager->createInstance('non_existing');
      $this->fail('Drupal\Component\Plugin\Exception\ExceptionInterface expected');
    }
    catch (ExceptionInterface $e) {
      $this->pass('Drupal\Component\Plugin\Exception\ExceptionInterface expected and caught.');
    }
    catch (Exception $e) {
      $this->fail('Drupal\Component\Plugin\Exception\ExceptionInterface expected, but ' . get_class($e) . ' was thrown.');
    }
  }

  /**
   * Test that the Reflection factory can create a plugin instance.
   *
   * The mock plugin classes use different values for their constructors
   * allowing us to test the reflection capabilities as well.
   *
   * We use derivative classes here because the block test type has the
   * reflection factory and it provides some additional variety in plugin
   * object creation.
   */
  function testReflectionFactory() {
    // Ensure a non-derivative plugin can be instantiated.
    $plugin = $this->mockBlockManager->createInstance('user_login', array('title' => 'Please enter your login name and password'));
    $this->assertIdentical(get_class($plugin), 'Drupal\plugin_test\Plugin\plugin_test\mock_block\MockUserLoginBlock', 'Correct plugin class instantiated.');
    $this->assertIdentical($plugin->getTitle(), 'Please enter your login name and password', 'Plugin instance correctly configured.');

    // Ensure a derivative plugin can be instantiated.
    $plugin = $this->mockBlockManager->createInstance('menu:main_menu', array('depth' => 2));
    $this->assertIdentical($plugin->getContent(), '<ul><li>1<ul><li>1.1</li></ul></li></ul>', 'Derived plugin instance correctly instantiated and configured.');

    // Ensure that attempting to instantiate non-existing plugins throws a
    // PluginException. Test this for a non-existing base plugin, a non-existing
    // derivative plugin, and a base plugin that may not be used without
    // deriving.
    foreach (array('non_existing', 'menu:non_existing', 'menu') as $invalid_id) {
      try {
        $this->mockBlockManager->createInstance($invalid_id);
        $this->fail('Drupal\Component\Plugin\Exception\ExceptionInterface expected');
      }
      catch (ExceptionInterface $e) {
        $this->pass('Drupal\Component\Plugin\Exception\ExceptionInterface expected and caught.');
      }
      catch (Exception $e) {
        $this->fail('An unexpected Exception of type "' . get_class($e) . '" was thrown with message ' . $e->getMessage());
      }
    }
  }
}
