<?php

/**
 * @file
 * Definition of Drupal\system\Tests\Plugin\PluginTestBase
 */

namespace Drupal\system\Tests\Plugin;

use Symfony\Component\ClassLoader\UniversalClassLoader;
use Drupal\simpletest\UnitTestBase;

/**
 * @todo
 */
abstract class PluginTestBase extends UnitTestBase {
  protected $loader;

  public function setUp() {
    parent::setUp();
    // We're a unit test using classes... don't let the db registry blow up.
    spl_autoload_unregister('drupal_autoload_class');
    spl_autoload_unregister('drupal_autoload_interface');
  }

  public function tearDown() {
    // Re-register autoloaders before tearDown begins.
    spl_autoload_register('drupal_autoload_interface');
    spl_autoload_register('drupal_autoload_class');

    parent::tearDown();
  }
}
