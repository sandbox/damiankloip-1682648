<?php

/**
 * @file
 * Definition of Drupal\system\Tests\Plugin\PluginTest
 */

namespace Drupal\system\Tests\Plugin;

use Drupal\plugin_test\Plugin\MockBlockManager;

/**
 * Tests the plugin system.
 */
class DerivativePluginTest extends PluginTestBase {
  protected $mockBlockManager;

  public static function getInfo() {
    return array(
      'name' => 'Derivative Discovery',
      'description' => 'Tests the the plugin system derivative integration.',
      'group' => 'Plugin API',
    );
  }

  public function setUp() {
    parent::setUp();

    // Real modules implementing plugin types may expose a module-specific API
    // for retrieving each type's plugin manager, or make them available in
    // Drupal's dependency injection container, but for unit testing, we get
    // the manager directly.
    $this->mockBlockManager = new MockBlockManager();
  }

  /**
   * Tests getDefinitions() and getDefinition() with a derivativeDecorator.
   */
  function testDerivativeDecorator() {

    // @see MockBlockManager::_construct().
    $expected = array(
      'user_login' => array(
        'label' => 'User login',
        'class' => 'Drupal\plugin_test\Plugin\plugin_test\mock_block\MockUserLoginBlock',
      ),
      'menu:main_menu' => array(
        'label' => 'Main menu',
        'class' => 'Drupal\plugin_test\Plugin\plugin_test\mock_block\MockMenuBlock',
      ),
      'menu:navigation' => array(
        'label' => 'Navigation',
        'class' => 'Drupal\plugin_test\Plugin\plugin_test\mock_block\MockMenuBlock',
      ),
      'layout' => array(
        'label' => 'Layout',
        'class' => 'Drupal\plugin_test\Plugin\plugin_test\mock_block\MockLayoutBlock',
      ),
      'layout:foo' => array(
        'label' => 'Layout Foo',
        'class' => 'Drupal\plugin_test\Plugin\plugin_test\mock_block\MockLayoutBlock',
      ),
    );

    // Ensure that getDefinitions() returns the expected definitions.
    $this->assertIdentical($this->mockBlockManager->getDefinitions(), $expected);

    // Ensure that getDefinition() returns the expected definition.
    foreach ($expected as $id => $definition) {
      $this->assertIdentical($this->mockBlockManager->getDefinition($id), $definition);
    }

    // Ensure that NULL is returned as the definition of a non-existing base
    // plugin, a non-existing derivative plugin, or a base plugin that may not
    // be used without deriving.
    $this->assertIdentical($this->mockBlockManager->getDefinition('non_existing'), NULL, 'NULL returned as the definition of a non-existing base plugin.');
    $this->assertIdentical($this->mockBlockManager->getDefinition('menu:non_existing'), NULL, 'NULL returned as the definition of a non-existing derivative plugin.');
    $this->assertIdentical($this->mockBlockManager->getDefinition('menu'), NULL, 'NULL returned as the definition of a base plugin that may not be used without deriving.');
  }
}
