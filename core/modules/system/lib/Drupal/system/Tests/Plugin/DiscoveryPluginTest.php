<?php

/**
 * @file
 * Definition of Drupal\system\Tests\Plugin\PluginTest
 */

namespace Drupal\system\Tests\Plugin;

use Drupal\plugin_test\Plugin\TestPluginManager;

/**
 * Tests the plugin system.
 */
class DiscoveryPluginTest extends PluginTestBase {
  protected $testPluginManager;

  public static function getInfo() {
    return array(
      'name' => 'Discovery API',
      'description' => 'Tests the the plugin system API.',
      'group' => 'Plugin API',
    );
  }

  public function setUp() {
    parent::setUp();

    // Real modules implementing plugin types may expose a module-specific API
    // for retrieving each type's plugin manager, or make them available in
    // Drupal's dependency injection container, but for unit testing, we get
    // the manager directly.
    $this->testPluginManager = new TestPluginManager();
  }

  /**
   * Tests getDefinitions() and getDefinition().
   */
  function testDiscoveryInterface() {

    // @see TestPluginManager::_construct().
    $expected = array(
      'user_login' => array(
        'label' => 'User login',
        'class' => 'Drupal\plugin_test\Plugin\plugin_test\mock_block\MockUserLoginBlock',
      ),
    );

    // Ensure that getDefinitions() returns the expected definitions.
    $this->assertIdentical($this->testPluginManager->getDefinitions(), $expected);

    // Ensure that getDefinition() returns the expected definition.
    foreach ($expected as $id => $definition) {
      $this->assertIdentical($this->testPluginManager->getDefinition($id), $definition);
    }

    // Ensure that NULL is returned as the definition of a non-existing plugin.
    $this->assertIdentical($this->testPluginManager->getDefinition('non_existing'), NULL, 'NULL returned as the definition of a non-existing base plugin.');
  }
}
