<?php

namespace Drupal\filter\Plugins\filter;

/**
 *
 */
class HtmlCorrector extends FilterPluginBase {
  function filter($text, $filter) {
    return filter_dom_serialize(filter_dom_load($text));
  }
}
