<?php

namespace Drupal\filter\Plugins\filter;

/**
 *
 */
class HtmlEscape extends FilterPluginBase {
  /**
   * Escapes all HTML tags, so they will be visible instead of being effective.
   */
  function filter($text, $filter) {
    return trim(check_plain($text));
  }

  /**
   * Filter tips callback for HTML escaping filter.
   */
  function filter_tips($filter, $format, $long = FALSE) {
    return t('No HTML tags allowed.');
  }

}
