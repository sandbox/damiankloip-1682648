<?php

namespace Drupal\filter\Plugins\filter;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Plugin\Plugin;

/**
 *
 */
abstract class FilterPluginBase extends PluginBase {

  /**
   *
   */
  protected $settings = array();

  public function __construct($settings) {
    $this->settings = $settings += $this->default_filter_settings();
  }

  abstract public function filter($text, $filter);

  function filter_settings($form, &$form_state, $filter, $format, $defaults) {
    return array();
  }

  function default_filter_settings() {
    return array();
  }

  function filter_tips($filter, $format, $long = FALSE) {
    return '';
  }

}
