<?php

namespace Drupal\filter\Plugins\filter;

/**
 *
 */
class Url extends FilterPluginBase {
  /**
   * Settings callback for URL filter.
   */
  function filter_settings($form, &$form_state, $filter, $format, $defaults) {
    parent::filter_settings($defaults);
  
    $settings['filter_url_length'] = array(
      '#type' => 'number',
      '#title' => t('Maximum link text length'),
      '#default_value' => $this->settings['filter_url_length'],
      '#min' => 1,
      '#field_suffix' => t('characters'),
      '#description' => t('URLs longer than this number of characters will be truncated to prevent long strings that break formatting. The link itself will be retained; just the text portion of the link will be truncated.'),
    );

    return $settings;
  }

  function default_filter_settings() {
    return array(
      'filter_url_length' => 72,
    );
  }
  
  /**
   * URL filter. Automatically converts text into hyperlinks.
   *
   * This filter identifies and makes clickable three types of "links".
   * - URLs like http://example.com.
   * - E-mail addresses like name@example.com.
   * - Web addresses without the "http://" protocol defined, like www.example.com.
   * Each type must be processed separately, as there is no one regular
   * expression that could possibly match all of the cases in one pass.
   */
  function filter($text, $filter) {
    // Tags to skip and not recurse into.
    $ignore_tags = 'a|script|style|code|pre';
  
    // Pass length to regexp callback.
    $this->trim(NULL, $this->settings['filter_url_length']);
  
    // Create an array which contains the regexps for each type of link.
    // The key to the regexp is the name of a function that is used as
    // callback function to process matches of the regexp. The callback function
    // is to return the replacement for the match. The array is used and
    // matching/replacement done below inside some loops.
    $tasks = array();
  
    // Prepare protocols pattern for absolute URLs.
    // check_url() will replace any bad protocols with HTTP, so we need to support
    // the identical list. While '//' is technically optional for MAILTO only,
    // we cannot cleanly differ between protocols here without hard-coding MAILTO,
    // so '//' is optional for all protocols.
    // @see filter_xss_bad_protocol()
    $protocols = variable_get('filter_allowed_protocols', array('http', 'https', 'ftp', 'news', 'nntp', 'telnet', 'mailto', 'irc', 'ssh', 'sftp', 'webcal', 'rtsp'));
    $protocols = implode(':(?://)?|', $protocols) . ':(?://)?';
  
    // Prepare domain name pattern.
    // The ICANN seems to be on track towards accepting more diverse top level
    // domains, so this pattern has been "future-proofed" to allow for TLDs
    // of length 2-64.
    $domain = '(?:[A-Za-z0-9._+-]+\.)?[A-Za-z]{2,64}\b';
    $ip = '(?:[0-9]{1,3}\.){3}[0-9]{1,3}';
    $auth = '[a-zA-Z0-9:%_+*~#?&=.,/;-]+@';
    $trail = '[a-zA-Z0-9:%_+*~#&\[\]=/;?!\.,-]*[a-zA-Z0-9:%_+*~#&\[\]=/;-]';
  
    // Prepare pattern for optional trailing punctuation.
    // Even these characters could have a valid meaning for the URL, such usage is
    // rare compared to using a URL at the end of or within a sentence, so these
    // trailing characters are optionally excluded.
    $punctuation = '[\.,?!]*?';
  
    // Match absolute URLs.
    $url_pattern = "(?:$auth)?(?:$domain|$ip)/?(?:$trail)?";
    $pattern = "`((?:$protocols)(?:$url_pattern))($punctuation)`";
    $tasks['parse_full_links'] = $pattern;
  
    // Match e-mail addresses.
    $url_pattern = "[A-Za-z0-9._-]{1,254}@(?:$domain)";
    $pattern = "`($url_pattern)`";
    $tasks['parse_email_links'] = $pattern;
  
    // Match www domains.
    $url_pattern = "www\.(?:$domain)/?(?:$trail)?";
    $pattern = "`($url_pattern)($punctuation)`";
    $tasks['parse_partial_links'] = $pattern;
  
    // Each type of URL needs to be processed separately. The text is joined and
    // re-split after each task, since all injected HTML tags must be correctly
    // protected before the next task.
    foreach ($tasks as $task => $pattern) {
      // HTML comments need to be handled separately, as they may contain HTML
      // markup, especially a '>'. Therefore, remove all comment contents and add
      // them back later.
      $this->escape_comments('', TRUE);

      $replace_callback = function($match) {
        return $this->escape_comments($match);
      };

      $text = preg_replace_callback('`<!--(.*?)-->`s', $replace_callback, $text);
  
      // Split at all tags; ensures that no tags or attributes are processed.
      $chunks = preg_split('/(<.+?>)/is', $text, -1, PREG_SPLIT_DELIM_CAPTURE);
      // PHP ensures that the array consists of alternating delimiters and
      // literals, and begins and ends with a literal (inserting NULL as
      // required). Therefore, the first chunk is always text:
      $chunk_type = 'text';
      // If a tag of $ignore_tags is found, it is stored in $open_tag and only
      // removed when the closing tag is found. Until the closing tag is found,
      // no replacements are made.
      $open_tag = '';
  
      for ($i = 0; $i < count($chunks); $i++) {
        if ($chunk_type == 'text') {
          // Only process this text if there are no unclosed $ignore_tags.
          if ($open_tag == '') {
            // If there is a match, inject a link into this chunk via the callback
            // function contained in $task.
            $chunks[$i] = preg_replace_callback($pattern, $task, $chunks[$i]);
          }
          // Text chunk is done, so next chunk must be a tag.
          $chunk_type = 'tag';
        }
        else {
          // Only process this tag if there are no unclosed $ignore_tags.
          if ($open_tag == '') {
            // Check whether this tag is contained in $ignore_tags.
            if (preg_match("`<($ignore_tags)(?:\s|>)`i", $chunks[$i], $matches)) {
              $open_tag = $matches[1];
            }
          }
          // Otherwise, check whether this is the closing tag for $open_tag.
          else {
            if (preg_match("`<\/$open_tag>`i", $chunks[$i], $matches)) {
              $open_tag = '';
            }
          }
          // Tag chunk is done, so next chunk must be text.
          $chunk_type = 'text';
        }
      }
  
      $text = implode($chunks);
      // Revert back to the original comment contents
      $this->escape_comments('', FALSE);

      $text = preg_replace_callback('`<!--(.*?)-->`', $replace_callback, $text);
    }
  
    return $text;
  }
  
  /**
   * preg_replace callback to make links out of absolute URLs.
   */
  function parse_full_links($match) {
    // The $i:th parenthesis in the regexp contains the URL.
    $i = 1;
  
    $match[$i] = decode_entities($match[$i]);
    $caption = check_plain($this->trim($match[$i]));
    $match[$i] = check_plain($match[$i]);
    return '<a href="' . $match[$i] . '">' . $caption . '</a>' . $match[$i + 1];
  }
  
  /**
   * preg_replace callback to make links out of e-mail addresses.
   */
  function parse_email_links($match) {
    // The $i:th parenthesis in the regexp contains the URL.
    $i = 0;
  
    $match[$i] = decode_entities($match[$i]);
    $caption = check_plain($this->trim($match[$i]));
    $match[$i] = check_plain($match[$i]);
    return '<a href="mailto:' . $match[$i] . '">' . $caption . '</a>';
  }
  
  /**
   * preg_replace callback to make links out of domain names starting with "www."
   */
  function parse_partial_links($match) {
    // The $i:th parenthesis in the regexp contains the URL.
    $i = 1;
  
    $match[$i] = decode_entities($match[$i]);
    $caption = check_plain($this->trim($match[$i]));
    $match[$i] = check_plain($match[$i]);
    return '<a href="http://' . $match[$i] . '">' . $caption . '</a>' . $match[$i + 1];
  }
  
  /**
   * preg_replace callback to escape contents of HTML comments
   *
   * @param $match
   *   An array containing matches to replace from preg_replace_callback(),
   *   whereas $match[1] is expected to contain the content to be filtered.
   * @param $escape
   *   (optional) Boolean whether to escape (TRUE) or unescape comments (FALSE).
   *   Defaults to neither. If TRUE, statically cached $comments are reset.
   */
  function escape_comments($match, $escape = NULL) {
    static $mode, $comments = array();
  
    if (isset($escape)) {
      $mode = $escape;
      if ($escape){
        $comments = array();
      }
      return;
    }
  
    // Replace all HTML coments with a '<!-- [hash] -->' placeholder.
    if ($mode) {
      $content = $match[1];
      $hash = md5($content);
      $comments[$hash] = $content;
      return "<!-- $hash -->";
    }
    // Or replace placeholders with actual comment contents.
    else {
      $hash = $match[1];
      $hash = trim($hash);
      $content = $comments[$hash];
      return "<!--$content-->";
    }
  }
  
  /**
   * Shortens long URLs to http://www.example.com/long/url...
   */
  function trim($text, $length = NULL) {
    static $_length;
    if ($length !== NULL) {
      $_length = $length;
    }
  
    // Use +3 for '...' string length.
    if ($_length && strlen($text) > $_length + 3) {
      $text = substr($text, 0, $_length) . '...';
    }
  
    return $text;
  }
  
  /**
   * Filter tips callback for URL filter.
   */
  function filter_tips($filter, $format, $long = FALSE) {
    return t('Web page addresses and e-mail addresses turn into links automatically.');
  }

}
