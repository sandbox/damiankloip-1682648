<?php

/**
 * @file
 * Definition of Drupal\filter\Plugins\Type\Filter.
 */

namespace Drupal\filter\Plugins\Type;

use Drupal\Component\Plugin\PluginManagerBase;
//use Drupal\Core\Plugin\Discovery\DirectoryDiscovery;
use Drupal\Core\Plugin\Discovery\HookDiscovery;
use Drupal\Component\Plugin\Factory\DefaultFactory;

/**
 *
 */
class Filter extends PluginManagerBase {
  public function __construct() {
    $this->discovery = new HookDiscovery('filter_info');
    $this->factory = new DefaultFactory($this);
  }
}
